import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-basic-modal',
  templateUrl: './basic-modal.component.html',
  styleUrls: ['./basic-modal.component.css']
})
export class BasicModalComponent implements OnInit {

  @Input() title: string;
  @Input() bodies: Array<string>;
  @Input() okVisible = false;
  @Input() cancelVisible = false;
  @Input() yesVisible = false;
  @Input() noVisible = false;
  @Input() closeVisible = false;
  @Input() crossVisible = false;

  constructor(
    private activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

}
