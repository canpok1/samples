import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutingPath } from './routing-path';
import { LoginComponent } from './login/login.component';
import { TopComponent } from './top/top.component';
import { AuthenticationService } from './service/authentication.service';

const routes: Routes = [
  { path: '', redirectTo: RoutingPath.LOGIN, pathMatch: 'full' },
  { path: RoutingPath.LOGIN, component: LoginComponent },
  { path: RoutingPath.TOP, component: TopComponent, canActivate: [AuthenticationService] },
  { path: '**', redirectTo: RoutingPath.LOGIN },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
