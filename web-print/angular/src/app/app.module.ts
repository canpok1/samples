import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrintSample1Component } from './print-sample-1/print-sample-1.component';
import { PrintListComponent } from './print-list/print-list.component';
import { PrintItemStorageService } from './print-item-storage.service';

@NgModule({
  declarations: [
    AppComponent,
    PrintSample1Component,
    PrintListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [PrintItemStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
