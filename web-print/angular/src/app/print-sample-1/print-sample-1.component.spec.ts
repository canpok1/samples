import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintSample1Component } from './print-sample-1.component';

describe('PrintSample1Component', () => {
  let component: PrintSample1Component;
  let fixture: ComponentFixture<PrintSample1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintSample1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintSample1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
