import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, ModalService } from '../service/services';
import { RoutingPath } from '../routing-path';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.css']
})
export class TopComponent implements OnInit {

  closeResult: string;

  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private modalService: ModalService
  ) { }

  ngOnInit() {
  }

  openCloseDialog() {
    this.modalService.openCloseModal(
      'タイトル',
      [
        '1行目',
        '2行目',
        '3行目',
        '4行目',
      ]
    );
  }

  openOkCancelDialog() {
    this.modalService.openOkCancelModal(
      'タイトル',
      [
        '1行目',
        '2行目',
        '3行目',
        '4行目',
      ]
    );
  }

  openYesNoDialog() {
    this.modalService.openYesNoModal(
      'タイトル',
      [
        '1行目',
        '2行目',
        '3行目',
        '4行目',
      ]
    );
  }

  logout() {
    this.auth.logout();
    this.router.navigate([RoutingPath.LOGIN]);
  }

}
