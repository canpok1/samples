package jp.gr.java_conf.ktnet.sample.spring_rest.entities;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Authentication {
    private User user;
    private String accessToken;
}