import { TestBed, inject } from '@angular/core/testing';

import { PrintItemStorageService } from './print-item-storage.service';

describe('PrintItemStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrintItemStorageService]
    });
  });

  it('should be created', inject([PrintItemStorageService], (service: PrintItemStorageService) => {
    expect(service).toBeTruthy();
  }));
});
