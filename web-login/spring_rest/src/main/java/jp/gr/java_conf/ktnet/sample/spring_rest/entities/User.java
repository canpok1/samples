package jp.gr.java_conf.ktnet.sample.spring_rest.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {

    private String name;

    @JsonIgnore
    private String password;
}