import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrintSample1Component } from './print-sample-1/print-sample-1.component';
import { PrintListComponent } from './print-list/print-list.component';

const routes: Routes = [
  { path: '', component: PrintListComponent },
  { path: 'sample1', component: PrintSample1Component },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
