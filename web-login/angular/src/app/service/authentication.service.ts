import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Authentication, Error } from '../model/models';
import { environment } from '../../environments/environment';
import { RoutingPath } from '../routing-path';

@Injectable()
export class AuthenticationService implements CanActivate {

  private static localStorageKey = 'authentication';

  constructor(
    private http: Http,
    private router: Router
  ) { }

  getAuthentication(): Authentication {
    return JSON.parse(localStorage.getItem(AuthenticationService.localStorageKey));
  }

  login(
    username: string,
    password: string,
    successCallback?: (response: Authentication) => void,
    failureCallback?: (response: Error, status?: number) => void
  ): void {
    console.log(`***** called(${username},${password}) *****`);

    const url = `${environment.apiBaseUrl}/login?username=${username}&password=${password}`;
    const body = 'username=' + username + '&password=' + password;

    this.http.post(url, body)
        .map((response: Response) => response.json())
        .subscribe(
          (response: Authentication) => {
            localStorage.setItem(AuthenticationService.localStorageKey, JSON.stringify(response));
            if (successCallback) {
              successCallback(response);
            }
          },
          (response) => {
            if (response._body && failureCallback) {
              failureCallback(JSON.parse(response._body), response.status);
            }
          });
  }

  logout(): void {
    localStorage.removeItem(AuthenticationService.localStorageKey);
  }

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const auth: Authentication = this.getAuthentication();
    if (auth) {
      return true;
    } else {
      this.router.navigate([RoutingPath.LOGIN]);
      return false;
    }
  }

}
