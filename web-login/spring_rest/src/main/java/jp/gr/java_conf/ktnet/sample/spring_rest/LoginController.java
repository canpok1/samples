package jp.gr.java_conf.ktnet.sample.spring_rest;

import java.util.ArrayList;
import java.util.List;
import jp.gr.java_conf.ktnet.sample.spring_rest.entities.Authentication;
import jp.gr.java_conf.ktnet.sample.spring_rest.entities.User;
import jp.gr.java_conf.ktnet.sample.spring_rest.exceptions.ApplicationException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    private List<Authentication> authList;
    
    public LoginController() {
        this.authList = new ArrayList<Authentication>();
        this.authList.add(makeAuth("user1", "password", "12345"));
    }

    @PostMapping(value = "/login")
    public Authentication login(
        @RequestParam String username,
        @RequestParam String password
    ) {
        for (Authentication auth : this.authList) {
            User user = auth.getUser();
            if (user.getName().equals(username)
                && user.getPassword().equals(password)
            ) {
                return auth;
            }
        }
        throw ApplicationException.makeLoginFailureException(username, password);
    }

    private Authentication makeAuth(String username, String password, String accessToken) {
        return Authentication.builder()
                .accessToken(accessToken)
                .user(User.builder().name(username).password(password).build())
                .build();
    }
}
