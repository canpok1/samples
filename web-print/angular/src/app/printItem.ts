export interface PrintItem {
    title: string;
    price: number;
    category: string;
    managementNo: string;
    message: string;
}
