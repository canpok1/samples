import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/services';
import { Authentication, Error } from '../model/models';
import { RoutingPath } from '../routing-path';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMessage: string = null;

  constructor(
    private router: Router,
    private auth: AuthenticationService,
  ) { }

  ngOnInit() {
  }

  onLogin(username: string, password: string): void {
    this.auth.login(
        username,
        password,
        (auth: Authentication) => {
          this.errorMessage = null;
          this.router.navigate([RoutingPath.TOP]);
        },
        (error: Error, status: number) => {
          this.errorMessage = error.message;
          console.log(`***** login failure[${status}][${error.message}]`);
        });
  }
}
