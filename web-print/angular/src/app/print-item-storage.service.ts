import { Injectable } from '@angular/core';
import { PrintItem } from './printItem';

@Injectable()
export class PrintItemStorageService {

  constructor() { }

  printItems: Array<PrintItem> = [];

  getPrintItems(): Array<PrintItem> {
    return this.printItems;
  }

  setPrintItems(printItems: Array<PrintItem>) {
    this.printItems = printItems;
  }
}
