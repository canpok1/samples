import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationService, ModalService } from './service/services';
import { TopComponent } from './top/top.component';
import { BasicModalComponent } from './modal/basic-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TopComponent,
    BasicModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [AuthenticationService, ModalService],
  bootstrap: [AppComponent],
  entryComponents: [BasicModalComponent]
})
export class AppModule { }
