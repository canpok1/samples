package jp.gr.java_conf.ktnet.sample.spring_security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SampleController {

    @RequestMapping("/")
    public String showRoot() {
        return "index";
    }

    @RequestMapping("/index")
    public String showIndex() {
        return "index";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }


}