import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalComponent } from '../modal/basic-modal.component';

@Injectable()
export class ModalService {

  constructor(
    private modal: NgbModal
  ) { }

  openOkCancelModal(
    title: string,
    bodies: Array<string>,
    okCallback?: () => void,
    cancelCallback?: () => void,
    closeCallback?: () => void
  ) {
    const modalRef = this.modal.open(BasicModalComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.bodies = bodies;
    modalRef.componentInstance.okVisible = true;
    modalRef.componentInstance.cancelVisible = true;
    modalRef.componentInstance.crossVisible = true;

    modalRef.result.then(
      (result) => {
        if (result === 'ok') {
          if (okCallback) { okCallback(); }
        } else if (result === 'cancel') {
          if (cancelCallback) { cancelCallback(); }
        } else {
          if (closeCallback) { closeCallback(); }
        }
      },
      (reason) => {
        if (closeCallback) { closeCallback(); }
      }
    );
  }

  openYesNoModal(
    title: string,
    bodies: Array<string>,
    yesCallback?: () => void,
    noCallback?: () => void,
    closeCallback?: () => void
  ) {
    const modalRef = this.modal.open(BasicModalComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.bodies = bodies;
    modalRef.componentInstance.yesVisible = true;
    modalRef.componentInstance.noVisible = true;
    modalRef.componentInstance.crossVisible = true;

    modalRef.result.then(
      (result) => {
        if (result === 'yes') {
          if (yesCallback) { yesCallback(); }
        } else if (result === 'no') {
          if (noCallback) { noCallback(); }
        } else {
          if (closeCallback) { closeCallback(); }
        }
      },
      (reason) => {
        if (closeCallback) { closeCallback(); }
      }
    );
  }

  openCloseModal(
    title: string,
    bodies: Array<string>,
    closeCallback?: () => void
  ) {
    const modalRef = this.modal.open(BasicModalComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.bodies = bodies;
    modalRef.componentInstance.closeVisible = true;

    modalRef.result.then(
      (result) => {
        if (closeCallback) { closeCallback(); }
      },
      (reason) => {
        if (closeCallback) { closeCallback(); }
      }
    );
  }
}
