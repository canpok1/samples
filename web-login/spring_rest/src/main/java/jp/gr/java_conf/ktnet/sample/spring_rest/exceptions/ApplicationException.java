package jp.gr.java_conf.ktnet.sample.spring_rest.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * 例外.
 */
@Data
public class ApplicationException extends RuntimeException {

    public static ApplicationException makeLoginFailureException(String username, String password) {
        return new ApplicationException(
                HttpStatus.FORBIDDEN,
                "ログインに失敗しました",
                null
                );
    }

    private HttpStatus httpStatus;

    private ApplicationException(HttpStatus httpStatus, String message, Throwable cause) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }
}