import { Component, OnInit } from '@angular/core';
import { PrintItemStorageService } from '../print-item-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-print-list',
  templateUrl: './print-list.component.html',
  styleUrls: ['./print-list.component.css']
})
export class PrintListComponent implements OnInit {

  constructor(
    private printItemStorage: PrintItemStorageService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onClickBtn() {
    const items = [];
    for (let index = 0; index < 20; index++) {
      items.push( {
        title: `タイトル${index}`,
        price: index * 100,
        category: `カテゴリ${index}`,
        managementNo: '012345678901234567890123456789',
        message: '◯◯付',
      });
    }
    this.printItemStorage.setPrintItems(items);

    this.router.navigate(['sample1']);
    // window.open('sample1');
  }
}
