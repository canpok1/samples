import { Component, OnInit, Input } from '@angular/core';
import { PrintItem } from '../printItem';
import { PrintItemStorageService } from '../print-item-storage.service';

@Component({
  selector: 'app-print-sample-1',
  templateUrl: './print-sample-1.component.html',
  styleUrls: ['./print-sample-1.component.css']
})
export class PrintSample1Component implements OnInit {

  static maxItemCount = 9;

  printItems: Array<Array<PrintItem>> = [];

  constructor(private printItemStorage: PrintItemStorageService) { }

  ngOnInit() {
    const items = this.printItemStorage.getPrintItems();

    this.printItems = [];
    let pageItems = [];
    items.forEach((item) => {
      if (pageItems.length === PrintSample1Component.maxItemCount) {
        this.printItems.push(pageItems);
        pageItems = [];
      }
      pageItems.push(item);
    });
    this.printItems.push(pageItems);
  }

}
