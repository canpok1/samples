import { User } from './models';

export interface Authentication {
    user: User;
    accessToken: string;
}
