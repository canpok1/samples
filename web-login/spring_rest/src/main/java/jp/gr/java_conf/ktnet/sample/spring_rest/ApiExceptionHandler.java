package jp.gr.java_conf.ktnet.sample.spring_rest;

import jp.gr.java_conf.ktnet.sample.spring_rest.entities.ErrorResponse;
import jp.gr.java_conf.ktnet.sample.spring_rest.exceptions.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
@Slf4j
public class ApiExceptionHandler {

    @ExceptionHandler
    public ErrorResponse handleException(Exception ex) {
        log.error("例外発生", ex);
        return ErrorResponse.builder().message("例外発生").build();
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleException(ApplicationException ex) {
        log.error("例外発生", ex);
        ErrorResponse response = ErrorResponse.builder().message(ex.getMessage()).build();
        return new ResponseEntity<>(response, ex.getHttpStatus());
    }

}