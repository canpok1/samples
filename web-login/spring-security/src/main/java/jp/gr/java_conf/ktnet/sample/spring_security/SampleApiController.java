package jp.gr.java_conf.ktnet.sample.spring_security;

import java.security.Principal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleApiController {
    @RequestMapping("/api/me")
    public String getMe(Principal principal) {
        return "{ 'username': '" + principal.getName() + "' }";
    }
}